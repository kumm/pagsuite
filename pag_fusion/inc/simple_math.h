#ifndef SIMPLE_MATH_H
#define SIMPLE_MATH_H

namespace simple_math {
    int pow(int b,int exp);
    double pow(double b,double exp);
    int fak(int x);
    double fak(double x);
}

#endif // SIMPLE_MATH_H
