#ifndef MT_TYPES_H
#define MT_TYPES_H

#include <inttypes.h>
typedef unsigned int uint;
typedef unsigned short ushort;
typedef unsigned short index_type;
//typedef long long int64_t;
typedef int l_int;
#define l_int_MAX INT_MAX

#include "mt_graph_types.h"

#endif // MT_TYPES_H
