#ifndef HEADERS_H
#define HEADERS_H

#include <vector>
#include <map>
#include <set>
#include <stdlib.h>
#include <time.h>
#include <list>
#include <iostream>
#include <string>
#include <climits>
#include <cmath>

using namespace std;

#include "mt_graph_types.h"
#include "mt_matree2.h"
#include "simple_math.h"
#include "mt_debug.h"

#endif // HEADERS_H
