#!/bin/bash
rm split_input.dot
rm rpag_solution.txt
rm pag_split_split_graph*
rm pag_fusion_input.txt
rm after_*.png
rm before_*.png
rm after_*.dot
rm before_*.dot
echo "# # # # # # # # # # #"
echo "CALLING: RPAG"
echo "# # # # # # # # # # #"
./rpag `cat weights.txt`  --file_output
echo "# # # # # # # # # # #"
echo "CALLING: PAG_SPLIT"
echo "# # # # # # # # # # #"
./pag_split `cat rpag_solution.txt | grep 'pipelined_adder_graph' | cut -d= -f2` `cat weights_split.txt` --pag_fusion_input
echo "# # # # # # # # # # #"
echo "CALLING: RPAG_FUSION"
echo "# # # # # # # # # # #"
./pag_fusion --if pag_fusion_input.txt --costmodel l --limit 6g --greedy_search_depth 8
oldp=$(pwd)
cd /home/ubuntu/flopoco/build
echo "# # # # # # # # # # #"
echo "CALLING: FLOPOCO"
echo "# # # # # # # # # # #"
./flopoco IntConstMultShiftAdd wIn=10 graph=$(cat "$oldp/pag_fusion_solution.txt")
cd "$oldp"
echo "# # # # # # # # # # #"
echo "rendering dot files"
echo "# # # # # # # # # # #"
exit
dot -Tpng split_input.dot -o split_input.png
dot -Tpng pag_fusion_solution.dot -o pag_fusion_solution.png
dot -Tpng pag_split_split_graph_1.dot -o pag_split_split_graph_1.png
dot -Tpng pag_split_split_graph_2.dot -o pag_split_split_graph_2.png
dot -Tpng pag_split_split_graph_3.dot -o pag_split_split_graph_3.png
dot -Tpng pag_split_split_graph_4.dot -o pag_split_split_graph_4.png
dot -Tpng pag_split_split_graph_5.dot -o pag_split_split_graph_5.png
dot -Tpng pag_split_split_graph_6.dot -o pag_split_split_graph_6.png
dot -Tpng pag_split_split_graph_7.dot -o pag_split_split_graph_7.png
dot -Tpng before_split.dot -o before_split.png
dot -Tpng before_inflate.dot -o before_inflate.png
dot -Tpng before_copy.dot -o before_copy.png
dot -Tpng after_copy.dot -o after_copy.png
dot -Tpng before__split.dot -o before__split.png
dot -Tpng after__split.dot -o after__split.png
dot -Tpng after__split1.dot -o after__split1.png
dot -Tpng after__split2.dot -o after__split2.png
dot -Tpng after__split3.dot -o after__split3.png
dot -Tpng after__split4.dot -o after__split4.png
dot -Tpng after__split5.dot -o after__split5.png
dot -Tpng after__split6.dot -o after__split6.png
echo "# # # # # # # # # # #"
echo "showing weight infos"
echo "# # # # # # # # # # #"
echo "weights in weights.txt" $(cat weights.txt | grep -o "[-1234567890]*" | wc -l)
cat weights_split.txt | sed 's/ /\n/g'  | while read -r line ; do
    echo "weights split with " $(echo $line | grep -o "[-1234567890]*" | wc -l) " elements"
    # your code goes here
done
