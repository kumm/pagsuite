cmake_minimum_required(VERSION 3.5)
PROJECT( pag_fusion CXX )

set( PAGSUITE_ROOT_DIR ${CMAKE_CURRENT_SOURCE_DIR}/.. )

include(CheckCXXCompilerFlag)
CHECK_CXX_COMPILER_FLAG("-std=c++11" COMPILER_SUPPORTS_CXX11)
CHECK_CXX_COMPILER_FLAG("-std=c++0x" COMPILER_SUPPORTS_CXX0X)
if(COMPILER_SUPPORTS_CXX11)
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
elseif(COMPILER_SUPPORTS_CXX0X)
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x")
else()
	message(STATUS "The compiler ${CMAKE_CXX_COMPILER} has no C++11 support. Please use a different C++ compiler.")
endif()

include_directories( ${PAGSUITE_ROOT_DIR}/paglib/inc )

include_directories(${CMAKE_CURRENT_SOURCE_DIR}/inc)
FILE( GLOB_RECURSE SOURCES ${CMAKE_CURRENT_SOURCE_DIR}/src/*.cpp)
FILE( GLOB_RECURSE HEADERS ${CMAKE_CURRENT_SOURCE_DIR}/inc/*.h)

ADD_EXECUTABLE( pag_fusion ${SOURCES} ${HEADERS})

set_target_properties( pag_fusion PROPERTIES LINKER_LANGUAGE CXX)

if( NOT PAG_LIB )
  #PAG_LIB is not defined,let's search for paglib
	FIND_LIBRARY( PAG_LIB NAMES pag )
	FIND_PATH( PAGSUITE_INCLUDE_DIR NAMES pagsuite/adder_graph.h )

	IF( PAG_LIB AND PAGSUITE_INCLUDE_DIR )
		ADD_DEFINITIONS(-DHAVE_PAGLIB)
		Message(STATUS "Found PAGSuite's paglib: ${PAG_LIB} (${PAGSUITE_INCLUDE_DIR})")
	ELSE( PAG_LIB AND PAGSUITE_INCLUDE_DIR )
		Message(FATAL_ERROR "Could not find PAGSuite's paglib (PAG_LIB=${PAG_LIB}, PAGSUITE_INCLUDE_DIR=${PAGSUITE_INCLUDE_DIR})")
	ENDIF(PAG_LIB AND PAGSUITE_INCLUDE_DIR )
ENDIF( NOT PAG_LIB )


TARGET_LINK_LIBRARIES( pag_fusion ${PAG_LIB} )

INSTALL(TARGETS pag_fusion
	RUNTIME DESTINATION bin
	LIBRARY DESTINATION lib
	ARCHIVE DESTINATION lib
)
