#include "parsers/inputparser.h"
#include <fstream>
#include <string.h>
#include <algorithm>

/// Removes all the whitespace left and right.
/// Like: "     non  whitespace    " -> "non  whitespace"
/// \param source Some dirty whitespaced stringiboy.
/// \return Same as the input! But different! But still the same! Just without leading and trailing whitespace!
string trim(const string &source) {
  std::string s(source);
  s.erase(0,s.find_first_not_of(" \n\r\t"));
  s.erase(s.find_last_not_of(" \n\r\t")+1);
  return s;
}

/// Given a text, prefix and suffix the function cuts out the string between the first found prexif and first found suffix.
/// Also the text is edited: The found prefix, suffix and the string between them is removed.
/// How strange... this function is quite fittingly to cut out an xml node like <h1>I will be the returned string!</h1>
/// cutoutContent("whitespace  <h1>swag</h1> white space <h1>another</h1>", "<h1>", "</h1>") returns: "swag".
/// \param text Text to cut out from.
/// \param contentPrefix The prefix to find.
/// \param contentSuffix The suffix to find.
/// \return The string between prefix and suffix.
string cutoutContent(string* text, const string contentPrefix, const string contentSuffix)
{
  //find the prefix
  string::size_type indexOfPrefixStart = text->find(contentPrefix);
  if(indexOfPrefixStart == string::npos)
    throw std::runtime_error("While cutting out content an error occurred. \"" + contentPrefix +
                             "\" the prefix was not present in the text: " + *text);
  if(text->find(contentSuffix) != string::npos)
  {
    //erase the prefix
    *text = text->erase(indexOfPrefixStart, contentPrefix.length());
    //find the suffix
    string::size_type indexOfContentEnd = text->find(contentSuffix);
    //cut out the content
    string content = text->substr(indexOfPrefixStart, indexOfContentEnd - indexOfPrefixStart);
    //erase the conent and suffix at once
    *text = text->erase(indexOfPrefixStart, indexOfContentEnd - indexOfPrefixStart + contentSuffix.length());
    return content;
  }
  else
    throw std::runtime_error("While cutting out content an error occurred. \"" + contentSuffix +
                             "\" the suffix was not present in the text " + *text);
}

/// converts some text to a vector<string> via given delimiters.
/// textToSet("a,b;c", ",;") returns: "a", "b", "c"
/// \param text the textual list like "asd, lol jk, hello; other delimiters".
/// \param delimiters All valid delimiters for your list in one string like ";,".
/// \return The vector of strings generated from the textual representation.
vector<string> textToSet(string* text, const string delimiters)
{
  vector<string> set;
  size_t pos = 0;
  string token;
  // Code from: https://stackoverflow.com/questions/7621727/split-a-string-into-words-by-multiple-delimiters
  while ((pos = text->find_first_of(delimiters)) != std::string::npos) {
    token = text->substr(0, pos);
    token = trim(token);
    if(!token.empty())
      set.push_back(token);
    text->erase(0, pos + 1);
  }
  token = *text;
  token = trim(token);
  if(!token.empty())
    set.push_back(token);
  return set;
}

/// Takes FLAT! xml as a string like "<a>..</a>..." and pops the first element thereby erasing it from the xml string.
/// \param nodeName Reference to save the name of the popped node. Will be an empty string if none xml node was found.
/// \param xml The FLAT! xml string.
/// \return The content of the popped node or an empty string if none xml node was found.
string flatXMLPop(string* nodeName, string* xml)
{
  string content;
  if (xml->find_first_of("<") != string::npos &&
      xml->find_first_of(">") != string::npos &&
      xml->find_first_of("</") != string::npos)
  {
    // erase whitespace and other invalid stuff before the first node
    *xml = xml->erase(0, xml->find_first_of("<"));
    // find, save and cut out the next node
    *nodeName = xml->substr(1, xml->find_first_of(">") - 1); // Cut out the nodename "<nodeName>" -> "nodeName"
    // find, save and cut out the content
    content = cutoutContent(xml, "<"+ *nodeName + ">", "</"+*nodeName+">");
    //*xml = xml->substr(xml->find(endNodeName) + endNodeName.length()); // now throw away the content and the closing node </nodeName>. We really do not need that anymore!
    return content;
  }
  else
  {
    // there is no xml node in this string
    *nodeName = "";
    return "";
  }
}

InputParser::InputParser(const string &filename){
  cout << "Filename: " << filename << endl;
  ifstream file(filename.c_str());
  std::string line;
  while( getline(file,line) ){
    line.erase(std::remove(line.begin(), line.end(), '\r'), line.end()); //remove carriage return if Windows encoding is used!
    if( line.find('#')!=string::npos ){
      input_file_content += line.substr(0,line.find('#'));
    }else{
      input_file_content += line;
    }
  }
  file.close();
}

bool InputParser::parse(){

  //this method fills InputParser::graphs with all the graphs found in the file
  cout << "input_file_content: " << input_file_content << endl;
  //!!!!!!!!!!!!!!!!!
  //!!!!!! static initialization to see how it works !!!!!! Data structures "graphs", "fixed_nodes" and "start_merging" have to be assigned by parsing "input_file_content"
  //!!!!!!!!!!!!!!!!!

  string content;
  string nodeName = "";
  do
  {
    // get the first xml entry like <graph>...</graph>
    // content is the part between the <graph> and </graph> nodes
    content = flatXMLPop(&nodeName, &input_file_content);
    if(nodeName == "graph") {
      graphs.push_back(content);
    }
    else if(nodeName == "merge") {
      // 123;543;412 -> [123, 543, 412]
      vector<vector<int64_t>> result;
      string content_cpy = content; //necessary as textToSet changes the string
      vector<string> strs = textToSet(&content_cpy, ",;");

      for(string s : strs)
      {
        result.push_back({stoll(s)});
      }
      start_merging.push_back(result);


      //store output values for later:
      vector<vector<int64_t>> output;
      content_cpy = content; //necessary as textToSet changes the string
      vector<string> confs = textToSet(&content_cpy, ";");

      for(auto s1 : confs)
      {
        vector<int64_t> values;
        vector<string> valuestr = textToSet(&s1, ",");
        for(auto s2 : valuestr)
        {
          values.push_back({stoll(s2)});
        }
        output.push_back(values);
      }

      outputs.push_back(output);
    }
    else if(nodeName == "fixed") {
      //{{[345],3,}} -> [345],3,
      content = cutoutContent(&content, "{{", "}}");
      // [345],3, -> 345
      string outputsStr = cutoutContent(&content, "[", "]");
      vector<string> outputsStrList = textToSet(&outputsStr, ",;");
      vector<int64_t> outputsList;
      for(string s : outputsStrList)
        outputsList.push_back(stoll(s));
      int stage = stoll(textToSet(&content, ",;").front());
      fixed_nodes.push_back({ .outputs = {outputsList}, .stage = stage });
    }
  } while (!nodeName.empty());


//  graphs.push_back("{{'A',[123],3,[69],2,1,[-15],2,0},{'A',[345],3,[69],2,0,[69],2,2},{'A',[15],2,[1],1,4,[-1],1,0},{'A',[69],2,[1],1,0,[17],1,2},{'R',[1],1,[1],0},{'A',[17],1,[1],0,0,[1],0,4}}");
//  graphs.push_back("{{'A',[321],3,[15],2,0,[153],2,1},{'A',[543],3,[153],2,2,[-69],2,0},{'A',[15],2,[1],1,4,[-1],1,0},{'A',[69],2,[1],1,0,[17],1,2},{'A',[153],2,[17],1,0,[17],1,3},{'R',[1],1,[1],0},{'A',[17],1,[1],0,0,[1],0,4}}");
//  graphs.push_back("{{'A',[412],3,[1],2,10,[-153],2,2},{'A',[654],3,[15],2,6,[-153],2,1},{'R',[1],2,[1],1},{'A',[15],2,[1],1,4,[-1],1,0},{'A',[153],2,[17],1,0,[17],1,3},{'R',[1],1,[1],0},{'A',[17],1,[1],0,0,[1],0,4}}");
//
//  fixed_nodes.push_back({ .outputs = {{123LL}}, .stage = 3 });
//  fixed_nodes.push_back({ .outputs = {{543LL}}, .stage = 3 });
//  fixed_nodes.push_back({ .outputs = {{412LL}}, .stage = 3 });
//  fixed_nodes.push_back({ .outputs = {{345LL}}, .stage = 3 });
//  fixed_nodes.push_back({ .outputs = {{321LL}}, .stage = 3 });
//  fixed_nodes.push_back({ .outputs = {{654LL}}, .stage = 3 });
//
//  start_merging.push_back({{123LL},{543LL},{412LL}});
//  start_merging.push_back({{345LL},{321LL},{654LL}});

/*  The old parser using CoPa:

    CoPa graphs = between("<graph>",(CoPa(PARSE_STR,0)),"</graph>") + event(0);

    CoPa merge_single = splitBy(",",CoPa(PARSE_INT,1)) + event(1);
    CoPa merge_cfg = splitBy(";",merge_single);
    CoPa merges = merge_cfg.between("<merge>","</merge>") + event(5);

    CoPa fixed_single = (("DONT_CARE" + event(4)) | splitBy(",",(CoPa(PARSE_INT,2))) + event(2));
    CoPa fixed_cfg = splitBy(";",fixed_single);
    CoPa fixed_node = print() + fixed_cfg.between("[" ,"]") + "," + CoPa(PARSE_UINT,3) + event(3);
    CoPa fixed_nodes = "{" + splitBy(",",fixed_node.between("{","}")) + "}";
    CoPa fixes = fixed_nodes.between( "<fixed>","</fixed>");

    CoPa toplevel = ignore(" \t\r\n") + repeat(graphs,'+') + repeat(merges,'+') + (fixes|"");
    InputParser_Scope *scope = new InputParser_Scope;
    scope->parser = this;
    toplevel.setScope( scope );

    //toplevel.print();

    return toplevel.parse( input_file_content );
*/
  return true;
}


void InputParser_Scope::onEvent(int id)
{
/*
    switch(id){
    case 0: //graphs finished
    {
        CoPa_Value val = getValueSingle(0);
        parser->graphs.push_back(val.toString());
        clearValueGroup(0);
    }
        break;
    case 1: //merge_single finished
    {
        std::vector<CoPa_Value> val_gr = getValueGroup(1);
        std::vector<int64_t> outputs;
        for(int i=0;i<val_gr.size();++i){
            outputs.push_back( val_gr.at(i).toInt() );
        }
        cur_merge_node.push_back(outputs);
        clearValueGroup(1);
    }
        break;
    case 2: //fixed_single finished
    {
        std::vector<CoPa_Value> val_gr = getValueGroup(2);
        std::vector<int64_t> outputs;
        for(int i=0;i<val_gr.size();++i){
            outputs.push_back( val_gr.at(i).toInt() );
        }
        cur_fix_node.outputs.push_back(outputs);
        clearValueGroup(2);
    }
        break;
    case 3: //merge_single finished
    {
        cur_fix_node.stage = getValueSingle(3).toInt();
        parser->fixed_nodes.push_back( cur_fix_node );
        cur_fix_node.stage = 0;
        cur_fix_node.outputs.clear();
        clearValueGroup(3);
    }
        break;

    case 4: //merge_single finished
    {
        cur_fix_node.outputs.push_back(std::vector<int64_t>());
    }
        break;
    case 5: //merge_single finished
    {
        parser->start_merging.push_back( cur_merge_node );
        cur_merge_node.clear();
    }
        break;
    }
*/
}

void InputParser_Scope::onExit(){
}
