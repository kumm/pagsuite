#include "mt_limits.h"

const unsigned int mt_limits::limit_cfgs = 28;
const unsigned int mt_limits::limit_nodes[] ={
    0,0,100,100,
    100,48,25,16,
    11,8,6,5,5,4,
    3,3,3,3,
    2,2,2,2,
    2,2,2,2,
    2,2,2
};
