#include "pagsuite/adder_graph.h"
#include <stdlib.h>
#include <math.h>

#ifdef HAVE_GMPXX
#include <gmp.h>
#include <gmpxx.h>
#endif

namespace PAGSuite
{
    int computeWordSize(std::vector<std::vector<int64_t> > &output_factor, int wIn)
    {
#ifdef HAVE_GMPXX
        mpz_class max_sum{0};
#else
        int64_t max_sum = 0;
#endif
        //for ech configuration c
        for(unsigned c = 0; c < output_factor.size(); ++c)
        {
#ifdef HAVE_GMPXX
            mpz_class sumOfFactors{0};
            mpz_class sumOfAbsFactors{0};
#else
            int64_t sumOfAbsFactors = 0;
#endif
            //for each element
            for(unsigned j = 0; j < output_factor[c].size(); ++j)
            {
                if (output_factor[c][j] != DONT_CARE)
                {
#ifdef HAVE_GMPXX
                  sumOfFactors += mpz_class(std::to_string(output_factor[c][j]));
                  sumOfAbsFactors += mpz_class(std::to_string(abs(output_factor[c][j])));
#else
                    sumOfAbsFactors += output_factor[c][j];
#endif
                }
            }
            if (sumOfAbsFactors > max_sum)
            {
                max_sum = sumOfAbsFactors;
                if(sumOfFactors < 0)
                {
                  //now, check is sumOfFactors is a power of two:
                  mpz_class d=-sumOfFactors;
                  mpz_class r;
                  do{
                    r=d % 2; //is d divisable by two?
                    if(r == 0)
                      d = d / 2; //yes, divide by two
                  } while(r == 0);
                  //now, d is odd, if d==1, sumOfFactors was a power-of-two
                  if(d == 1)
                  {
                    max_sum++; //in the special case that the sum of factors is a negative power-of-two, we need value more due to the asymmetry in two's complement                }
                  }
                }
            }

        }

        if (abs(max_sum) <= 1) {
          return wIn;
        }
        max_sum -= 1; //-1 as mpz_sizeinbase(x,2) computes log_2(x+1)

        return mpz_sizeinbase(max_sum.get_mpz_t(), 2) + wIn;
    }

    int computeWordSize(adder_graph_base_node_t *node, int wIn)
    {
        int wOut = computeWordSize(node->output_factor, wIn);

        int wInNodeMax=0;
        if (is_a<adder_subtractor_node_t>(*node))
        {
            adder_subtractor_node_t *t = (adder_subtractor_node_t *) (node);
            if (t->input_shifts.front() < 0)
            {
              wOut += abs(t->input_shifts.front());
            }
            else
            {
              for (int i = 0; i < t->input_shifts.size(); i++)
              {
                int wInNode = t->input_shifts[i] + computeWordSize(t->inputs[i],wIn);
                wInNodeMax = wInNodeMax > wInNode ? wInNodeMax : wInNode;
              }
            }
        }
        if (is_a<conf_adder_subtractor_node_t>(*node))
        {
            conf_adder_subtractor_node_t *t = (conf_adder_subtractor_node_t *) (node);
            if (t->input_shifts.front() < 0)
            {
              wOut += abs(t->input_shifts.front());
            }
            else
            {
              for (int i = 0; i < t->input_shifts.size(); i++)
              {
                int wInNode = t->input_shifts[i] + computeWordSize(t->inputs[i],wIn);
                wInNodeMax = wInNodeMax > wInNode ? wInNodeMax : wInNode;
              }
            }
        }
        wOut = wOut > wInNodeMax ? wOut : wInNodeMax; //adjust output word size in case the required input word size is larger than the necessary output word size (in case of subtraction)
        return wOut;
    }

}
