cmake_minimum_required(VERSION 3.5)
PROJECT( osr CXX )

SET( PAGSUITE_ROOT_DIR ${CMAKE_CURRENT_SOURCE_DIR}/.. )

if( NOT PAG_LIB )
  #PAG_LIB is not defined,let's search for paglib
	FIND_LIBRARY( PAG_LIB NAMES pag )
	FIND_PATH( PAGSUITE_INCLUDE_DIR NAMES pagsuite/adder_graph.h )

	IF( PAG_LIB AND PAGSUITE_INCLUDE_DIR )
		SET(PAGLIB_FOUND true)
		ADD_DEFINITIONS(-DHAVE_PAGLIB)
		Message(STATUS "Found PAGSuite's paglib: ${PAG_LIB} (${PAGSUITE_INCLUDE_DIR})")
	ELSE( PAG_LIB AND PAGSUITE_INCLUDE_DIR )
		Message(FATAL_ERROR "Could not find PAGSuite's paglib (PAG_LIB=${PAG_LIB}, PAGSUITE_INCLUDE_DIR=${PAGSUITE_INCLUDE_DIR})")
	ENDIF(PAG_LIB AND PAGSUITE_INCLUDE_DIR )
ENDIF( NOT PAG_LIB )

IF(NOT BUILD_COMPLETE_PAGSUITE)
	#search for ScaLP
	FIND_LIBRARY(SCALP_LIB
		NAMES libScaLP libScaLP.so libScaLP.dylib
		HINTS "${SCALP_PREFIX_PATH}/lib"
		DOC "Directory of the ScaLP library")
	
	FIND_PATH(SCALP_H
		ScaLP/Solver.h
		  HINTS "${SCALP_PREFIX_PATH}/include"
		  DOC "Path to main ScaLP header file Solver.h"
	)
	IF(SCALP_H AND SCALP_LIB)
		MESSAGE(STATUS "Found ScaLP: ${SCALP_H},${SCALP_LIB}")
	ELSE (SCALP_H AND SCALP_LIB)
		MESSAGE(FATAL_ERROR "osr can not be build because ScaLP library was not found or provided. Install ScaLP and specify path by setting DCMAKE_PREFIX_PATH or SCALP_PREFIX_PATH (i.e., -DSCALP_PREFIX_PATH=<path to scalp install dir>)")
	ENDIF(SCALP_H AND SCALP_LIB)

ENDIF(NOT BUILD_COMPLETE_PAGSUITE)

IF (SCALP_H AND SCALP_LIB)
	include_directories( ${PAGSUITE_ROOT_DIR}/osr/inc ${PAGSUITE_INCLUDE_DIR} ${SCALP_H})
	
	FILE( GLOB_RECURSE SOURCES ${CMAKE_CURRENT_SOURCE_DIR}/src/*.cpp)
	FILE( GLOB_RECURSE HEADERS ${CMAKE_CURRENT_SOURCE_DIR}/inc/*.hpp)
	
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
	
	ADD_EXECUTABLE( osr ${SOURCES} ${HEADERS})
	
	target_link_libraries(osr ${PAG_LIB} ${SCALP_LIB})
	
	
	INSTALL(TARGETS osr
		RUNTIME DESTINATION bin
		LIBRARY DESTINATION lib
		ARCHIVE DESTINATION lib
	)
ELSE (SCALP_H AND SCALP_LIB)
	MESSAGE(WARNING "osr can not be build because ScaLP library was not found or provided.")
ENDIF (SCALP_H AND SCALP_LIB)

