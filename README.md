# Welcome to the PAGsuite project

## About the PAGsuite project 

The suite contains optimization tools for the (pipelined) multiple constant multiplication ((P)MCM) problem, i.e., the multiplication of a single variable with multiple constants using bit-shifts, registered adders/subtractors and registers, such that a register is placed after each addition/subtraction and the pipeline is valid.
Including the pipeline registers in the optimization is extremly usefull for FPGA-based designs but is also benefically for ASIC designs [1-3]. As PMCM is a generalization of the MCM problem (without pipelining), it can also be used to find MCM solutions with minimal adder depth.

Several people contributed to PAGsuite (see list of publications below).

Several algorithms are provided with PAGSuite which are briefly introduced in the following:

## The tools and libraries in PAGsuite
### RPAG  

RPAG solves the PMCM problem in a heuristic way. The algorithm was initially proposed in [2] where all fundamental ideas of the algorithm are presented. It is a heuristic approach that is based on a greedy search. Due to its fast implementation a second stage of optimization is possible to enhance the optimization results which are either based on random selection [2] or again a greedy search. It was later extended to include embedded FPGA multiplers for larger word sizes as needed for single precision floating point [4]. This work was outdated by [5] which can incoporate a given number of embedded multipliers of a given size for any word size. Applications are to reduce the number of embedded multipliers in large multipliers (e.g., as needed in floating point) or to compensate for a limited number of embedded multipliers in an efficient way. 

A recent extension allows the usage of ternary adders, i.e., adders with three inputs as available on modern FPGAs of Xilinx and Altera [6].

### OSCM

OSCM, stands for _Optimal Single Constant Mulitplication_ and provides optimal solutions (i.e., minimum adder solutions) for the single constant mulitplication (SCM) problem. It is an implementation of the work presented in [13].


### OMCM

OSCM, stands for _Optimal Multiple Constant Mulitplication_ and provides optimal solutions for the MCM problem. 
It is based on an ILP model proposed in [14] and uses [ScaLP](https://digidev.digi.e-technik.uni-kassel.de/scalp) as a wrapper to several ILP solvers.

### PAG fusion

PAG fusion solves the problem of finding run-time reconfigurable constant coefficient multipliers (RCCMs). These are multiplication circuits in which the output constant can be chosen from a predefined set of constants during run-time. This is achieved by the insertion of multiplexers into constant multiplication circuits. Doing this a reduction in the required resources can be achieved by  a reuse of redundant partial circuits. The problem which is solved is to find the best possible solution with a minimal fusing multiplexer overhead. An optimal approach was proposed in [8], but had to be supplemented by a heuristic and many other optimizations to enable reconfigurable MCM (RMCM) generation for relevant problem sizes [9]. The helper tool pag\_split can be used to create efficient adder graphs that have many adder nodes in common.
The benchmark coefficients used in this paper can be found in [DAGfusionTest.csv](https://gitlab.com/kumm/pagsuite/-/blob/master/benchmarks/DAGfusionTest.csv) and the output text files of DAG fusion can be downloaded within [DAGfusionTestILPInput.zip](https://gitlab.com/kumm/pagsuite/-/blob/master/benchmarks/DAGfusionTestILPInput.zip).

### OSR 

The Optimal Shift Reassignment (OSR) method [11] is based on the idea that shifts can be allocated at different places along the circuit, while the calculated output constant stays the same. In the context of RCCMs, this is new and differs from previous approaches, which were limited by the fact that all the constants within the constant multiplier were forced to be odd. However, the OSR method releases this restriction. As a result, the number of required multiplexers in the circuit can be reduced. This happens when the shift reassignment makes several inputs of a multiplexer correspond to the same shift of a certain signal. The source code dependens on [ScaLP](https://digidev.digi.e-technik.uni-kassel.de/scalp) as a wrapper to several ILP solvers. 


# Download & Installation

The git contains the current development snapshot. The sources can be built using cmake & make as usual:

    mkdir build
    cd build
    cmake ..
    make
    
In case you only want to built a single tool, you can do the same in any of the sub-folders.
You can use and/or modify it for research purposes (please ask for push access to make it accessable!), for commercial use, please ask for a license.

Most sources do not depend on any library and can be compiled on any platform (MacOS, Linux and Windows are tested for RPAG, MacOS and Linux are tested for OPAG) using gcc or clang (gcc 4.2 64bit, clang Apple LLVM version 5.1 are tested). 

All rights reserved

# Usage

## RPAG Usage

After compilation, type

`./rpag`

to get a list of arguments and some examples.

For a simple PAG optimization of, e.g., the coefficients {1 15 255 423} (like for the adder graph above), simply type 

`rpag 1 15 255 423`

The output will be


	used target set:1 15 255 423
	pipeline_set_best={[1 3],[1 47],[1 15 255 423]}
	pipelined_adder_graph={{'R',[1],1,[1],0},{'A',[3],1,[1],0,0,[1],0,1},{'R',[1],2,[1],1},{'A',[47],2,[3],1,4,[-1],1,0},{'R',[1],3,[1],2},{'A',[15],3,[1],2,4,[-1],2,0},{'A',[255],3,[1],2,8,[-1],2,0},{'A',[423],3,[47],2,0,[47],2,3},{'O',[1],3,[1],3,0},{'O',[15],3,[15],3,0},{'O',[255],3,[255],3,0},{'O',[423],3,[423],3,0}}
	pag_cost=8
	no_of_pipeline_stages=3
	no_of_registered_ops=8
	no_of_registers=3
	no_of_adders=5
	pag_cost=8

pipeline\_set\_best contains the nodes which are necessary to compute the target set. In the example, the coefficients in the first stage have to be 1 and 3 to compute 1 and 47 in the second stage which are needed to compute all the output coefficients in the last stage. 

pipelined\_adder\_graph contains the complete information to build an adder graph including stage and bit shift informations.  For example, the string `{'A',[47],2,[3],1,4,[-1],1,0}` means that 47 in stage 2 is computed from 3 in stage 1 shifted by 4 bit to the left minus 1 in stage 1 without shift, i.e., 47=3*2^4-1. For the full description of the syntax, see appendix of [Konrad Möller's PhD thesis](https://portal.ub.uni-kassel.de/kup/d/9783737603768).
I can be passed to other tools within PAGSuite or be passed to [FloPoCo](https://flopoco.org) to generate synthesizable VHDL code.

The graph syntax is valid Matlab syntax. To visualize the solution, Matlab scripts are provided (see 'Matlab Tools' section below) to create figures like this:

![](doc/MCM_adder_graph.png)

To get an idea what the algorithm is doing, increase the verbose level by, e.g.,

`rpag --verbose=1` 

To activate the use of ternary adders in addition to two-input adders (algorithm RPAGT, [5]), use the argument --ternary\_adders. The example in Fig. 3 of [5] can be obtained by:

`rpag --ternary_adders 20406 7567` 

A constant matrix multiplication (CMM) can be optimized by using the --cmm flag and by changing each constant to a sum/difference of constants. To compute, e.g., 123*x1+321*x2 345*x1+543*x2, RPAG has to be called like in the following:

`rpag --cmm 123+321 345+543`

The support of embedded multipliers in the adder graph [3,4] is not supported anymore, but available in RPAG v1.30. To do so, the maximum number of multipliers and their word size (at coefficient site) has to be given, e.g.,

`rpag --max_no_of_mult=1 --mult_wordsize=17 11280171 13342037`

would optimize for the coefficients [11280171 13342037] while a single multiplier of 17 bit is used. The results can be further enhanced by specifying the adder depth as well as the word size constraints for each stage, e.g.,

`rpag --mult_wordsize=17 --wordsize_constraints=[-1 -1 17 24] --adder_depth_constraints=[1 2 3 -1] --max_no_of_mult=1 11280171 13342037`

would do the same optimization as above but the wordsize is constrained to be 17 bit in stage 3 (-1 means "don't care") while the adder depth is only constrained in the stages 1-3. This reduces the number of stages by one, but does not guarantee a solution for the given constraints.


### Additional Matlab Scripts

To get nice plots of the pipelined adder graphs, the function

`write_pipelined_realization_graph_dot('graph.dot',pipelined_adder_graph)`

is provided in the `matlab` folder. pipelined\_adder\_graph is the cell array as provided by RPAG or OPAG. It can be used to generate a graphviz (www.graphviz.org) .dot file. Either their graphical user interface can be used or a postscript of pdf file can be created by typing:

`dot -ograph.ps -Tps2 graph.dot`

For convenience, the complete procedure including opening the resulting pdf (after conversion from ps) is done in matlab using:

`plot_pipelined_realization_graph(pipelined_adder_graph)`

To use it on your system, you have to set a temporary pdf path (variable pdf\_path), the system command for deleting files (variable delete\_cmd), the commands and paths in `dot2pdf.m` and the pdf viewer in showpdf.m.

## PAG fusion Usage

PAG fusion consists of two separate tools and requires RPAG solutions as inputs.

So first of all RPAG has to executed for example to generate a CMM circuit:

`./rpag 123 345 543 654 321 412 --file_output`

The next step is to extract the adder graph of the resulting rpag\_solution.txt which has the following example content:

    pipeline_set_best={[1 17],[1 15 69 153],[103 123 321 327 345 543]}
    pipelined_adder_graph={{'R',[1],1,[1],0},{'A',[17],1,[1],0,0,[1],0,4},{'R',[1],2,[1],1},{'A',[15],2,[1],1,4,[-1],1,0},{'A',[69],2,[1],1,0,[17],1,2},{'A',[153],2,[17],1,0,[17],1,3},{'A',[103],3,[1],2,8,[-153],2,0},{'A',[123],3,[69],2,1,[-15],2,0},{'A',[321],3,[15],2,0,[153],2,1},{'A',[327],3,[15],2,5,[-153],2,0},{'A',[345],3,[69],2,0,[69],2,2},{'A',[543],3,[153],2,2,[-69],2,0},{'O',[123],3,[123],3,0},{'O',[321],3,[321],3,0},{'O',[345],3,[345],3,0},{'R',[412],3,[103],3},{'O',[543],3,[543],3,0},{'R',[654],3,[327],3}}
    pag_cost=12

As we are interested in the adder graph we have to copy the string after "pipelined\_adder\_graph=" or simply read it with command line tools:

`echo `cat rpag\_solution.txt | grep 'pipelined\_adder\_graph' | cut -f2 -d=

Now the tool pag\_split has to be called to prepare the RPAG/OPAG solution for the fusion. To do so, the user has to provide the adder\_graph and the desired output mapping. For example 123 543 and 412 are 3 configurations of the first output adder and 345 321 and 654 are 3 configurations of the second output adder. The configurations are separated with an ";" and the different MCM outputs with white space:

    ./pag_split "{{'R',[1],1,[1],0},{'A',[17],1,[1],0,0,[1],0,4},{'R',[1],2,[1],1},{'A',[15],2,[1],1,4,[-1],1,0},{'A',[69],2,[1],1,0,[17],1,2},{'A',[153],2,[17],1,0,[17],1,3},{'A',[103],3,[1],2,8,[-153],2,0},{'A',[123],3,[69],2,1,[-15],2,0},{'A',[321],3,[15],2,0,[153],2,1},{'A',[327],3,[15],2,5,[-153],2,0},{'A',[345],3,[69],2,0,[69],2,2},{'A',[543],3,[153],2,2,[-69],2,0},{'O',[123],3,[123],3,0},{'O',[321],3,[321],3,0},{'O',[345],3,[345],3,0},{'O',[412],3,[103],3,2},{'O',[543],3,[543],3,0},{'O',[654],3,[327],3,1}}" "123;543;412 345;321;654" --pag_fusion_input

The resulting file "pag\_fusion\_input.txt" can then be used as input file for pag\_fusion

`./pag_fusion --if pag_fusion_input.txt --costmodel l`

The solution graph is the following

`{{'A',[17;17;17],1,[1;1;1],0,0,[1;1;1],0,4},{'R',[1;1;1],1,[1;1;1],0},{'R',[1;1;1],2,[1;1;1],1},{'A',[15;15;15],3,[1;1;1],2,4,[-1;-1;-1],2,0},{'M',[17;17;0],2,[17;17;17],1,[0;0;NaN]},{'A',[69;69;1],3,[1;1;1],2,0,[17;17;0],2,2},{'R',[NaN;17;17],2,[17;17;17],1},{'A',[NaN;153;153],3,[NaN;17;17],2,0,[NaN;17;17],2,3},{'M',[69;306;306],4,[69;69;1],3,[0;NaN;NaN],[NaN;153;153],3,[NaN;1;1]},{'M',[15;69;1024],4,[15;15;15],3,[0;NaN;NaN],[69;69;1],3,[NaN;0;10]},{'A',[123;543;412],5,[69;306;-306],4,1,[-15;-69;1024],4,0},{'M',[69;15;960],4,[69;69;1],3,[0;NaN;NaN],[15;15;15],3,[NaN;0;6]},{'M',[138;153;153],4,[69;69;1],3,[1;NaN;NaN],[NaN;153;153],3,[NaN;0;0]},{'A',[345;321;654],5,[69;15;960],4,0,[138;153;-153],4,1},{'O',[123;543;412],5,[123;543;412],5},{'O',[345;321;654],5,[345;321;654],5}}`


The resulting RMCM circuit can be found in the figure below.

![](doc/RMCM_adder_graph.png)


## Code as zip-file  

The current release is version 1.80 and can be downloaded as source distribution below.

[PAGSuite v1.80](https://gitlab.com/kumm/pagsuite/-/raw/master/releases/pagsuite_1_80.zip)


## Changelog 

RPAG v1.30 (released 26. June 2013): 
First public release containing support for pipelined adder graph optimization with common adders or ternary adders and the possibility to include embedded multipliers in the adder graphs. 

RPAG v1.40 (released 15. May 2014): 
Support for constant matrix multiplication (CMM).
Computation of the pipelined adder graph without additional matlab script (--show\_adder\_graph flag).
Cost models for minimum adder depth graphs (--cost\_model=min\_ad\_hl\_fpga and min_ad_ll_fpga cost models)
Note that the support for embedded multipliers was disabled in this version (--max\_no\_of\_mult option). 

[PAGSuite v1.50](https://gitlab.com/kumm/pagsuite/-/raw/master/releases/pagsuite_1_50.zip) (released 19. June 2019)
) (released 01. Oct 2015): 
OPAG included and renamed to PAGSuite
RPAG now supports ternary adders in the CMM optimization

[PAGSuite v1.60](https://gitlab.com/kumm/pagsuite/-/raw/master/releases/pagsuite_1_60.zip) (released 02. March 2016):
pag fusion is included in PAGSuite

[PAGSuite v1.70](https://gitlab.com/kumm/pagsuite/-/raw/master/releases/pagsuite_1_70.zip) (released 10. November 2017):
Minor bugfixes in RPAG Matlab code for CMM Problems
Added pag\_muxilp, in which the Optimal Shift Reassignment approach is implemented (see comments above)
Added oscm, a tool to determine the optimal SCM solution (based on work of Gustafsson and data extracted from Hcub of the Spiral project (http://spiral.ece.cmu.edu/mcm/gen.html)
Added omcm, a novel tool to optimally solve MCM and SCM problems using ILP.

[PAGSuite v1.80](https://gitlab.com/kumm/pagsuite/-/raw/master/releases/pagsuite_1_80.zip) (released 19. June 2019):
Cleanup of whole suite, programs removed that are no longer maintained or does not fit into the cmake flow (hopefully, those tools will be merged back at some point in time):
pag\_fusion, pag\_muxilp, pag\_split removed, because not longer maintained
opag removed, because of old qmake built system and its dependency on or-tools

<!--
[PAGSuite v1.90](https://gitlab.com/kumm/pagsuite/-/raw/master/releases/pagsuite_1_90.zip) (comming in January 2022):
Further cleanup, pag\_fusion, pag\_muxilp (now osr), pag\_split brought back again.
-->

### Benchmarks 

For benchmarking MCM problems, the following data was used:

[[https://digidev.digi.e-technik.uni-kassel.de/pagsuite/export/31/pagsuite/trunk/benchmarks/mcm_rand_coeff_sets0.mat|mcm_rand_coeff_sets0.mat]]

The file is a Matlab .mat file containing MCM data for 1 and 100 coefficients with word sizes between 1 and 32 bit. For each combination, 100 instances are provided in the set. 
After the .mat file is loaded the call

`
coeffs = rand_coeff_sets{word_size,no_of_coeff}
`

results a cell array of 100 instances with "no_of_coeff" coefficients and "word_size" bits.
Now, coeffs{1} gives you the first instance, etc.

## Selected References 

[1]	Martin Kumm, "Multiple Constant Multiplication Optimizations for Field Programmable Gate Arrays", Dissertation, Defended: 30.10.2015, Published: 02.05.2016, Springer Wiesbaden, ISBN: 978-3-658-13322-1, [[http://doi.org/10.1007/978-3-658-13323-8|doi:10.1007/978-3-658-13323-8]]

[2]	M. Kumm, P. Zipf, M. Faust, and C.-H. Chang, “Pipelined Adder Graph Optimization for High Speed Multiple Constant Multiplication,” IEEE International Symposium on Circuits and Systems (ISCAS), pp. 49–52, 2012.

[3]	M. Kumm and P. Zipf, “High Speed Low Complexity FPGA-Based FIR Filters Using Pipelined Adder Graphs,” presented at the International Conference on Field-Programmable Technology (FPT), 2011, pp. 1–4.

[4]	M. Kumm, K. Liebisch, and P. Zipf, “Reduced Complexity Single and Multiple Constant Multiplication in Floating Point Precision,” International Conference on Field Programmable Logic and Application (FPL), 2012.

[5]	M. Kumm and P. Zipf, “Hybrid Multiple Constant Multiplication for FPGAs,” IEEE International Conference on Electronics, Circuits and Systems (ICECS), 2012, pp. 556–559.

[6]	M. Kumm, M. Hardieck, J. Willkomm, P. Zipf, and U. Meyer-Baese, “Multiple Constant Multiplication with Ternary Adders,” International Conference on Field Programmable Logic and Application (FPL), 2013, pp. 1-8.

[7]   M. Kumm, D. Fanghänel, K. Möller, P. Zipf & U. Meyer-Baese, “FIR Filter Optimization for Video Processing on FPGAs,” EURASIP Journal on Advances in Signal Processing (Springer), 2013, pp. 1-18.

[8]   K. Möller, M. Kumm, M. Kleinlein, P.Zipf, "Pipelined Reconfigurable Multiplication with Constants on FPGAs," Field Programmable Logic and Applications (FPL), 2014 24th International Conference on, Munich, 2014, pp. 1-6.

[9]   K. Möller, M. Kumm, M. Kleinlein, P.Zipf, "Reconfigurable Constant Multiplication for FPGAs", IEEE Transactions on Computer-Aided Design of Integrated Circuits and Systems, vol., no., pp., 2016

[10]   M. Kumm, M. Hardieck, P. Zipf, "Optimization of Constant Matrix Multiplication with Low Power and High Throughput", IEEE Transactions on Computers, 66(12), 2072–2080, 2017

[11]   K. Möller, M. Kumm, M. Garrido, P. Zipf, "Optimal Shift Reassignment in Reconfigurable Constant Multiplication Circuits", IEEE Transactions on Computer-Aided Design of Integrated Circuits and Systems (TCAD), 37(3), 710–714, 2018

[12]   M. Hardieck, M. Kumm, K. Möller, P. Zipf, "Constant Matrix Multiplication with Ternary Adders", IEEE International Conference on Electronics, Circuits and Systems (ICECS), 2018

[13] O. Gustafsson, A. G. Dempster, L. Wanhammar, "Extended Results for Minimum-Adder Constant Integer Multipliers", IEEE International Symposium on Circuits and Systems (ISCAS), 2002

[14] M. Kumm, "Optimal Constant Multiplication using Integer Linear Programming", IEEE Transactions on Circuits and Systems II: Express Briefs 65, 567–571 (2018)
  
